FROM node:10.16.0

MAINTAINER Steve George <stephengeorge495@gmail.com>

WORKDIR /src

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

CMD ["/bin/bash", "-c", "npm install; /src/node_modules/.bin/ng build --watch --extract-css"]
